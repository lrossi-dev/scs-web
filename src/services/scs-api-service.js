import axios from "axios"


const baseUrl = "http://localhost:8080";
const projectsBasePath = "/projects"
const rulesBasePath = "/rules"
const scansBasePath = "/scans"

export const getAllProjects = () => {
    return axios.get(`${baseUrl}${projectsBasePath}`)
        .then(res => {
            if (res.status !== 200) {
                return [];
            }
            return res.data;
        })
        .catch(err => console.log(err));
}

export const newProject = (project) => {
    return axios.post(`${baseUrl}${projectsBasePath}`, project)
}

export const newScan = project => {
    return axios.post(`${baseUrl}${scansBasePath}`, project)
}

export const getScansByProject = (projectId) => {
    return axios.get(`${baseUrl}${projectsBasePath}/${projectId}/scans`)
        .then(res => res.data)
        .catch(err => console.log(err))
}

export const getScanDetails = scanId => {
    return axios.get(`${baseUrl}${scansBasePath}/${scanId}`)
        .then(res => res)
        .catch(err => console.log(err))
}
