import { Button, ButtonGroup, Card } from "react-bootstrap";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "./project-item.css"
import { useNavigate } from "react-router-dom";
import { newScan } from "../../services/scs-api-service";

export const ProjectItem = (props) => {
    const navigate = useNavigate()
    console.log("Project=" + props.project)
    return (
        <Card className="project-item-card">
            <Card.Body className="project-item-card-body">
                <b>{props.project.name}</b>
                <p>Path: {props.project.path}</p>
                <ButtonGroup>
                    <Button onClick={() => scanProject(props.project)}><b>SCAN</b></Button>
                    <Button variant="outline-primary" onClick={() => listScans(props.project.id)}><b>DETAILS</b></Button>
                </ButtonGroup>
            </Card.Body>
        </Card>
    )

    function scanProject(project) {
        toast(`Starting scan for ${project.name}. Go to project details to get more info`)
        newScan(project)
            .then(res => {
                if (res && res.headers && res.headers['Location']) {
                    const location = res.headers['Location'].split('/')
                    toast(`Scan #${location[location.length -1]} finished successfully! Go to project details to get more info.`)
                }
            })
            .catch(err => {
                let msg = `Error starting scan. Try again in a moment.`
                if (err.response && err.response.data && err.response.data.error.includes('no such file')) {
                    msg = 'File or directory not found'
                }
                toast.error(msg)
            })
    }
    
    function listScans(projectId) {
        navigate(`/project/${projectId}/scans`, {replace: true, state: { project: props.project }})
    }
}

