import { Badge,  Button,  Card } from "react-bootstrap";
import moment from "moment";
import './scan-item.css'
import { useNavigate } from 'react-router-dom'

export const ScanItem = (props) => {
    const navigate = useNavigate()
    return (
        <div >
            <Card className="scan-item-card">
                <Card.Body className="scan-item-card-body">
                    <b>{props.scan.id}</b>
                    <p>{moment(props.scan.date).format('yyyy-MM-DD hh:mm:ss')}</p>
                    <Badge bg={badgeColor(props.scan.status)} className="scan-item-badge">{props.scan.status}</Badge>
                    <Button variant="primary" onClick={() => getDetails(props.projectId, props.scan.id)}>DETAILS</Button>
                </Card.Body>
            </Card>
        </div>
    )
    
    function badgeColor(status) {
        switch(status) {
            case 'RUNNING': return 'warning';
            case 'FAILED': return 'danger';
            case 'SUCCESS': return 'success';
            default: return 'secondary'
        }
    }
    
    function getDetails(projectId, scanId) {
        navigate(`${scanId}/details`)
    }
}


