import { useState, useEffect } from "react"
import { getScanDetails } from "../../services/scs-api-service"
import { Table } from "react-bootstrap"
import { useParams } from "react-router-dom"
import './violations-page.css'

export const ViolationsPage = (props) => {
    const [details, setDetails] = useState({ violations: [] })
    const {scanId} = useParams()

    useEffect(() => {
        function populate(scanId) {
            getScanDetails(scanId)
                .then(res =>{
                    res && setDetails(res.data)
                })
                .catch(err => console.log(err))
        }
        populate(scanId)
        console.log(details)
    }, [])
    
    return (
        <Table striped bordered hover className="violations-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>File</th>
                    <th>Line</th>
                    <th>Rule</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                { details.violations.map(violation => (
                    <tr>
                        <td>{violation.id}</td>
                        <td>{violation.file}</td>
                        <td>{violation.line}</td>
                        <td>{violation.rule.name}</td>
                        <td>{violation.rule.description}</td>
                    </tr>
                ))}
            </tbody>
      </Table>
    )
}