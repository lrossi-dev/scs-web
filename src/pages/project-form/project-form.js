import { Form, Button } from "react-bootstrap"
import { useState } from "react"
import {toast} from 'react-toastify'
import './project-form.css'
import { newProject } from "../../services/scs-api-service"
import { useNavigate } from "react-router-dom"

export const ProjectForm = (props) => {
    const [name, setName] = useState('')
    const [path, setPath] = useState('')

    const navigate = useNavigate()

    const handleSubmit = (event) => {
        event.preventDefault()
        console.log(`name=${name},path=${path}`)
        newProject({ "name": name, "path": path})
            .then(res => {
                console.log(res)
                toast(`Project ${name} created`)
            })
            .catch(err => {
                console.log(err)
                toast.error('Unable to create project')
            })
            .finally(() => {
                navigate('/')
            })
    }

    return (
        <Form className="project-form-container" onSubmit={(e) => handleSubmit(e)}>
            <Form.Group className="mb-3" controlId="name">
                <Form.Label>Project name</Form.Label>
                <Form.Control type="text" placeholder="my-sample-project" value={name} onChange={(e) => setName(e.target.value)}/>
            </Form.Group>

            <Form.Group className="mb-3" controlId="path">
                <Form.Label>Path to Project</Form.Label>
                <Form.Control type="text" placeholder="/home/$USER/my-sample-project" value={path} onChange={(e) => setPath(e.target.value)}/>
            </Form.Group>
            <Button variant="primary" type="submit">
                CREATE
            </Button>
        </Form>
    )
}