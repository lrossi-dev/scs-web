import { Button } from "react-bootstrap"
import { ProjectItem } from "../../components/project-item/project-item"
import { getAllProjects } from "../../services/scs-api-service"
import { useEffect, useState } from "react"
import './projects-page.css'
import { useNavigate } from "react-router-dom"

export default function ProjectsPage(props) {
    const [projects, setProjects] = useState([])
    const navigate = useNavigate()
    useEffect(() => {
        async function populate() {
            await getAllProjects().then(res => {
                if (res) {
                    setProjects(res)
                    console.log(projects)
                }
            })
        }
        populate()
    }, [])
    return (
        <div style={{'margin-top': '1rem'}}>
            { 
                projects && projects.length > 0 ?
                (
                    <div>
                        <div className="new-project-row">
                            <h2>Your Projects</h2>
                            <Button variant="success" className="new-project-button" onClick={() => navigate('/project/new')}><b>NEW PROJECT</b></Button>
                        </div>
                        {projects.map(proj => (<ProjectItem project={proj}/>))}
                    </div>
                    
                )
                    
                : 
                (
                    <div className="row align-center">
                        <p>No projects scanned yet</p>
                        <Button className="no-projects-button" onClick={() => navigate('/project/new')}>Scan your first project!</Button>
                    </div>
                )
            }
        </div>
    )
}