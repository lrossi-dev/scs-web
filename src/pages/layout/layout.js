import { Nav, Navbar, Container } from "react-bootstrap"
import { Outlet } from "react-router-dom"

export const Layout = () => {
    return (
        <>
            <Navbar expand="lg" className="bg-body-tertiary">
                <Container style={{'justify-content': 'space-between'}}>
                    <Navbar.Brand href="/"><b>SCS</b></Navbar.Brand>
                    <Nav style={{ 'justify-content': 'right'}}>
                        <Nav.Link href="#home">Projects</Nav.Link>
                    </Nav>
                </Container>
            </Navbar>
            <div className='row'>
                <div className='col-md-3'></div>
                <div className='col-md-6'>
                    <Outlet/>
                </div>
                <div className='col-md-3'></div>
            </div>
        </>
    )
}