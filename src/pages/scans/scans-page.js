import { useEffect, useState } from "react"
import { getScansByProject, newScan } from "../../services/scs-api-service"
import { Button } from "react-bootstrap"
import { ScanItem } from "../../components/scan-item/scan-item"
import { useNavigate, useParams } from "react-router-dom"
import { toast } from "react-toastify"
import './scans-page.css'

export const ScansPage = (props, state) => {
    const [scans, setScans] = useState([])
    const {id} = useParams()
    const navigate = useNavigate()
    useEffect(() => {
        function populate(projectId) {
            getScansByProject(projectId)
                .then(data => setScans(data))
        }
        populate(id)
    }, [])

    return (
        <div style={{ 'margin-top': '2rem'}}>
            { 
                scans && scans.length > 0 ?
                (
                    <div>
                        <div className="new-scan-row">
                            <h2>Scans for project #{id}</h2>
                            <Button variant="primary" className="new-scan-button" onClick={() => scanNewProject(state.project)}><b>NEW SCAN</b></Button>
                        </div>
                        {scans.map(scan => (<ScanItem scan={scan} projectId={id}/>))}
                    </div>
                    
                )
                    
                : 
                (
                    <div className="row" style={{ "text-align": "center"}}>
                        <p>No scans for this project</p>
                        <Button><b>SCAN</b></Button>
                    </div>
                )
            }
        </div>
    )

    function scanNewProject(project) {
        toast(`Starting scan #${project.id}. Go to project details to get more info`)
        newScan(project)
            .then(res => {
                if (res && res.headers && res.headers['Location']) {
                    const location = res.headers['Location'].split('/')
                    toast(`Scan #${location[location.length -1]} finished successfully! Go to project details to get more info.`)
                }
            })
            .catch(err => {
                let msg = `Error starting scan. Try again in a moment.`
                if (err.response && err.response.data && err.response.data.error.includes('no such file')) {
                    msg = 'File or directory not found'
                }
                toast.error(msg)
            })
    }


}