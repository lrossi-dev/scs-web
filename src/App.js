import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import { Layout } from './pages/layout/layout';
import ProjectsPage from './pages/projects/projects-page';
import { ScansPage } from './pages/scans/scans-page';
import { ProjectForm } from './pages/project-form/project-form';
import { ViolationsPage } from './pages/violations/violations-page';

function App() {
  return (
    <>
        <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout/>}>
            <Route index element={<ProjectsPage/>} />
            <Route path='/project/:id/scans' element={<ScansPage/>}/>
            <Route path='/project/new' element={<ProjectForm/>}/>
            <Route path='/project/:id/scans/:scanId/details' element={<ViolationsPage/>}/>
          </Route>
        </Routes>
      </BrowserRouter>
      <ToastContainer/>
    </>
  );
}

export default App;
