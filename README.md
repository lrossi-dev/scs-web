# scs-web

WEB App to consume [scs-api](https://gitlab.com/lrossi-dev/scs-api).

## How to start

Install dependencies: `npm i`

Run app: `npm run start`

Of course the back-end should be running for correct behavior.
